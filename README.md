libwakeproto
=============

Wake protocol library for Qt5.

Wake is a protocl similar to SLIP. Protocol used in embedded applications.

Description (HTML): http://caxapa.ru/lib/wake/

Description (PDF): http://digit-el.com/files/open/wake/dn_wake/wake.pdf
